﻿using System.Threading.Tasks;
using GmailClient.Services;
using Microsoft.AspNetCore.Mvc;

namespace GmailClient.Web.Controllers
{
    public class GmailServiceController : Controller
    {
        // GET: GmailServiceController
        public async Task<ActionResult> Index()
        {
            var gmailService = new GmailService(null);
            var emails = await gmailService.GetEmails();

            return View(emails);
        }

    }
}
