﻿using System.Threading.Tasks;
using GmailClient.Models;
using Microsoft.AspNetCore.Mvc;

namespace GmailClient.Web.Controllers
{

    public class EmailSenderController : Controller
    {
        private readonly IMailService _emailSender;
        

        public EmailSenderController(IMailService emailSender)
        {
            this._emailSender = emailSender;
           
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Contact(MailRequest contact)
        {


            ViewData["Message"] = null;
            ViewData["Error"] = null;

            if (ModelState.IsValid)
            {
                

                await _emailSender.SendEmailAsync(contact);



                ViewData["message_color"] = "green";
                ViewData["Message"] = "*Your Message Has Been Sent.";
            }
            else
            {
                ViewData["message_color"] = "red";
                ViewData["Message"] = "*Please complete the required fields";
            }

            ModelState.Clear();

            return View(contact);
        }


        //[HttpPost("welcome")]
        //public async Task<IActionResult> SendWelcomeMail(WelcomeRequest request)
        //{
        //    try
        //    {
        //        await _emailSender.SendWelcomeEmailAsync(request);

        //        return View();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

    }

    //
    //[Route("api/[controller]")]
    //[ApiController]
    //public class EmailSenderController : ControllerBase
    //{
    //    private readonly IEmailSender _emailSender;
    //    public EmailSenderController(IEmailSender emailSender)
    //    {
    //        this._emailSender = emailSender;
    //    }
    //    [HttpPost("send")]
    //    public async Task<IActionResult> SendMail([FromForm] MailRequest request)
    //    {
    //        try
    //        {
    //            await _emailSender.SendEmailAsync(request);
    //            return Ok();
    //        }
    //        catch (Exception ex)
    //        {
    //            throw;
    //        }

    //    }


    //    [HttpPost("welcome")]
    //    public async Task<IActionResult> SendWelcomeMail([FromForm] WelcomeRequest request)
    //    {
    //        try
    //        {
    //            await _emailSender.SendWelcomeEmailAsync(request);
    //            return Ok();
    //        }
    //        catch (Exception ex)
    //        {
    //            throw;
    //        }
    //    }

    //}
}


