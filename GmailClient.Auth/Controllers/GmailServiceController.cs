﻿using System.Linq;
using System.Threading.Tasks;
using GmailClient.Services;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GmailClient.Auth.Controllers
{
    public class GmailServiceController : Controller
    {

        [Authorize]
        public async Task<ActionResult> Index([FromServices] IGoogleAuthProvider auth)
        {
            if (await auth.RequireScopesAsync(Google.Apis.Gmail.v1.GmailService.Scope.MailGoogleCom) is IActionResult authResult)
            {
                // If the required scopes are not granted, then a non-null IActionResult will be returned,
                // which must be returned from the action. This triggers incremental authorization.
                // Once the user has granted the scope, an automatic redirect to this action will be issued.
                return (ActionResult)authResult;
            }


            var gmailService = new GmailService(null);
            var emails = await gmailService.GetEmails();

            return View(emails);
        }



        [Authorize]
        public async Task<IActionResult> CalendarList([FromServices] IGoogleAuthProvider auth)
        {
            // Check if the required scopes have been granted. 
            if (await auth.RequireScopesAsync(CalendarService.ScopeConstants.CalendarReadonly) is IActionResult authResult)
            {
                // If the required scopes are not granted, then a non-null IActionResult will be returned,
                // which must be returned from the action. This triggers incremental authorization.
                // Once the user has granted the scope, an automatic redirect to this action will be issued.
                return authResult;
            }
            // The required scopes have now been granted.
            GoogleCredential cred = await auth.GetCredentialAsync();
            var service = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            var calendars = await service.CalendarList.List().ExecuteAsync();
            var calendarIds = calendars.Items.Select(calendar => calendar.Id).ToList();
            return View(calendarIds);
        }


    }
}
