﻿using GmailClient.Models;
using Google.Apis.Auth;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Drive.v3;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using MimeKit;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessagePart = Google.Apis.Gmail.v1.Data.MessagePart;
using Microsoft.IdentityModel.Tokens;



namespace GmailClient.Auth.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Public home page.
        /// No authorization required. User doesn't need to login to see this.
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Login action.
        /// No authentication specific code. Just adding the <see cref="AuthorizeAttribute"/>
        /// will trigger authentication if necessary.
        /// </summary>
        [Authorize]
        public IActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Logout action.
        /// Does nothing if the user is not logged in.
        /// </summary>
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            return View();
        }

        /// <summary>
        /// Shows the current scopes associated to the authenticated account.
        /// Specifying the <see cref="AuthorizeAttribute"/> will guarantee that the code
        /// executes only if the user is authenticated. No specific scopes are required.
        /// </summary>
        /// <param name="auth">The Google authorization provider.
        /// This can also be injected on the controller constructor.</param>
        [Authorize]
        public async Task<IActionResult> ScopeListing([FromServices] IGoogleAuthProvider auth)
        {
            IReadOnlyList<string> currentScopes = await auth.GetCurrentScopesAsync();
            return View(currentScopes);
        }

        /// <summary>
        /// Lists the authenticated user's Google Drive files.
        /// Specifying the <see cref="GoogleScopedAuthorizeAttribute"> will guarantee that the code
        /// executes only if the user is authenticated and has granted the scope specified in the attribute
        /// to this application.
        /// </summary>
        /// <param name="auth">The Google authorization provider.
        /// This can also be injected on the controller constructor.</param>
        [GoogleScopedAuthorize(DriveService.ScopeConstants.DriveReadonly)]
        public async Task<IActionResult> DriveFileList([FromServices] IGoogleAuthProvider auth)
        {
            GoogleCredential cred = await auth.GetCredentialAsync();
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            var files = await service.Files.List().ExecuteAsync();
            var fileNames = files.Files.Select(x => x.Name).ToList();
            return View(fileNames);
        }

        [GoogleScopedAuthorize(GmailService.ScopeConstants.MailGoogleCom)]
        public async Task<IActionResult> GmailMessageList([FromServices] IGoogleAuthProvider auth, string folder = "INBOX")
        {

            IList<Email> emails = new List<Email>();
            try
            {
                GoogleCredential credential = await auth.GetCredentialAsync();

                //TODO: save this credentials to DB
                //var fileDataStore = new FileDataStore(GetType().ToString());



                var gmailService = new Google.Apis.Gmail.v1.GmailService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "GmailClient"
                });

                var emailListRequest = gmailService.Users.Messages.List("me");
                emailListRequest.LabelIds = folder;
                emailListRequest.IncludeSpamTrash = false;
                //emailListRequest.Q = "is:unread"; // This was added because I only wanted unread emails...

                // Get our emails
                var emailListResponse = await emailListRequest.ExecuteAsync();

                if (emailListResponse != null && emailListResponse.Messages != null)
                    // Loop through each email and get what fields I want...
                    foreach (var message in emailListResponse.Messages)
                    {
                        var messageInfoRequest = gmailService.Users.Messages.Get("me", message.Id);

                        // Make another request for that email id...
                        var messageInfoResponse = await messageInfoRequest.ExecuteAsync();

                        if (messageInfoResponse != null)
                        {

                            var from = "";
                            var date = "";
                            var subject = "";
                            var body = "";


                            // Loop through the headers and get the fields I need...
                            foreach (var messagePartHeader in messageInfoResponse.Payload.Headers)
                            {
                                switch (messagePartHeader.Name)
                                {
                                    case "Date":
                                        date = messagePartHeader.Value;
                                        break;
                                    case "From":
                                        from = messagePartHeader.Value;
                                        break;
                                    case "Subject":
                                        subject = messagePartHeader.Value;
                                        break;
                                }

                                if (date != "" && from != "")
                                {
                                    if (messageInfoResponse.Payload.Parts == null &&
                                        messageInfoResponse.Payload.Body != null)
                                    {
                                        body = messageInfoResponse.Payload.Body.Data;
                                        // Need to replace some characters as the data for the email's body is base64

                                        var codedBody = body.Replace("-", "+");
                                        codedBody = codedBody.Replace("_", "/");
                                        var data = Convert.FromBase64String(codedBody);
                                        body = Encoding.UTF8.GetString(data);
                                    }
                                    else
                                        body = GetDecodedNestedParts(messageInfoResponse.Payload.Parts, "");


                                    var email = new Email()
                                    {
                                        Id = message.Id,
                                        Date = date,
                                        From = from,
                                        Title = subject,
                                        IsReceived = true,
                                        Body = body
                                    };
                                    DateTime parsedDate;
                                    if (DateTime.TryParse(date, out parsedDate))
                                    {
                                        email.Date = parsedDate.ToShortDateString();
                                    }
                                    emails.Add(email);
                                }
                            }
                        }
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return View(emails);

        }



        public string GetDecodedNestedParts(IList<MessagePart> part, string data)
        {
            var bodyData = data;
            if (part == null) return bodyData;

            foreach (var messagePart in part)
                if (messagePart.Parts == null)
                {
                    if (messagePart.Body != null && messagePart.Body.Data != null)
                    {
                        var codedBody = messagePart.Body.Data.Replace("-", "+");
                        codedBody = codedBody.Replace("_", "/");
                        var decodedFromBase64 = Convert.FromBase64String(codedBody);
                        bodyData += Encoding.UTF8.GetString(decodedFromBase64);
                    }
                }
                else
                {
                    return GetDecodedNestedParts(messagePart.Parts, bodyData);
                }

            return bodyData;
        }




        //        [GoogleScopedAuthorize(GmailService.ScopeConstants.MailGoogleCom)]
        //        public async Task<IActionResult> SendEmailAsync([FromServices] IGoogleAuthProvider auth, MailRequest mailRequest)
        //        {
        //            try
        //            {
        //                GoogleCredential credential = await auth.GetCredentialAsync();

        //                var fileDataStore = new FileDataStore(GetType().ToString());



        //                var gmailService = new Google.Apis.Gmail.v1.GmailService(new BaseClientService.Initializer
        //                {
        //                    HttpClientInitializer = credential,
        //                    ApplicationName = "GmailClient"
        //                });


        //                var userEmail = User.FindFirst(ClaimTypes.Email)?.Value!;
        //                var userPassword = User.FindFirst(ClaimTypes.p)?.Value!;

        //                var email = new MimeKit.MimeMessage
        //                {
        //                    Sender = userEmail; //take igorflyuntdev
        //            };
        //            email.To.Add(MimeKit.MailboxAddress.Parse(mailRequest.ToEmail)); //take igorflyunt2
        //            email.Subject = mailRequest.Subject; //take theme
        //            var builder = new MimeKit.BodyBuilder();
        //            //TODO: check attachments
        //            //if (mailRequest.Attachments != null) //remove to another method or class
        //            //{
        //            //    foreach (var file in mailRequest.Attachments.Where(file => file.Length > 0))
        //            //    {
        //            //        byte[] fileBytes; //Ask Victor if this really needs to add filebytes in BodyBuilder, or his method builder.attachments can parse and add file without byte (read in documentation)
        //            //        await using (var ms = new MemoryStream())
        //            //        {
        //            //            await file.CopyToAsync(ms);
        //            //            fileBytes = ms.ToArray();
        //            //        }

        //            //        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
        //            //    }
        //            //}

        //            builder.HtmlBody = mailRequest.Body; //Need logic to add html page to body, but it works without html, work with just text from view
        //            email.Body = builder.ToMessageBody();

        //            using var smtp = new SmtpClient();
        //            await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
        //            await smtp.AuthenticateAsync(_mailSettings.Mail, _mailSettings.Password);
        //            await smtp.SendAsync(email);
        //            await smtp.DisconnectAsync(true);
        //        }
        //            catch (Exception e)
        //            {
        //                Console.WriteLine(e);
        //            }
        //}



        //[GoogleScopedAuthorize(GmailService.ScopeConstants.MailGoogleCom)]
        //public async Task<IActionResult> SendEmailAsync([FromServices] IGoogleAuthProvider auth, MailRequest mailRequest)
        //{

           
        //    //var userName = User.FindFirst(ClaimTypes.Name)?.Value!;
        //    var userEmail = User.FindFirst(ClaimTypes.Email)?.Value!;

        //    //Create Message
        //    MimeMessage message = new MimeMessage();

        //    //message.From.Add(new MailboxAddress(userName, userEmail));
        //    //message.To.Add(new MailboxAddress(mailRequest.DisplayName, mailRequest.ToEmail));

        //    message.From.Add(MailboxAddress.Parse(userEmail));
        //    message.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
        //    message.Subject = mailRequest.Subject;
        //    var mailBody = mailRequest.Body;
        //    var builder = new BodyBuilder();
        //    builder.HtmlBody = mailBody;
        //    message.Body = builder.ToMessageBody();

        //    //TODO: check attachments

        //    GoogleCredential credential = await auth.GetCredentialAsync();

        //    var gmailService = new GmailService(new BaseClientService.Initializer
        //    {
        //        HttpClientInitializer = credential,
        //        ApplicationName = "GmailClient"
        //    });

        //    var gmailSender = gmailService.Users.Messages.Send(message.ToString(), "me").Execute();


            //if (userEmail != null) mail.From = new MailAddress(userEmail);
            //mail.Subject = mailRequest.Subject;
            //mail.Body = mailRequest.Body;
            //mail.IsBodyHtml = true;
            ////string attImg = "C:\\Documents\\Images\\Tulips.jpg OR Any Path to attachment";
            ////mail.Attachments = mailRequest.Attachments;
            //mail.Attachments.Add(new Attachment(mailRequest.AttachmentPath));
            //mail.To.Add(new MailAddress(mailRequest.ToEmail));
            //MimeKit.MimeMessage mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(mail);

            //Message message = new Message();
            //message.Raw = Base64UrlEncode(mimeMessage.ToString());
            ////Gmail API credentials
            //UserCredential credential;


            //// Create Gmail API service.
            //var service = new GmailService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = credential,
            //    ApplicationName = "GmailClient",
            //});
            ////Send Email
            //var result = service.Users.Messages.Send(message, "me/OR UserId/EmailAddress").Execute();
        //}



        [GoogleScopedAuthorize(GmailService.ScopeConstants.MailGoogleCom)]
        public async Task<IActionResult> SendHtmlEmailAsync([FromServices] IGoogleAuthProvider auth, MailRequest mailRequest)
        {
            

            try
            {
                string userEmail = User.FindFirst(ClaimTypes.Email)?.Value!;
                string address = mailRequest.ToEmail;
                //Create Message
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(userEmail);
                mail.Subject = mailRequest.Subject;
                mail.Body = mailRequest.Body;
                mail.IsBodyHtml = true;
                //string attImg = "C:\\Documents\\Images\\Tulips.jpg OR Any Path to attachment";
                //mail.Attachments.Add(new Attachment(attImg));
                mail.To.Add(new MailAddress(address));
                MimeMessage mimeMessage = MimeMessage.CreateFromMailMessage(mail);

                Message message = new Message();
                message.Raw = Base64UrlEncode(mimeMessage.ToString());

                //Gmail API credentials
                GoogleCredential credential = await auth.GetCredentialAsync();

                // Create Gmail API service.
                var service = new GmailService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "GmailClient",
                });
                //Send Email
                var result = await service.Users.Messages.Send(message, "me").ExecuteAsync();

                //TODO:Create success send page
                return View(message);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

                //TODO:Create fail send page
            }

        }


        private static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }



        /// <summary>
        /// Lists the authenticated user's Calendars.
        /// Specifying the <see cref="AuthorizeAttribute"/> will guarantee that the code executes only if the
        /// user is authenticated.
        /// No scopes are required via attributes.
        /// Instead, scope are required via code using <see cref="IGoogleAuthProvider.RequireScopesAsync(string[])"/>.
        /// </summary>
        /// <param name="auth">The Google authorization provider.
        /// This can also be injected on the controller constructor.</param>
        [Authorize]
        public async Task<IActionResult> CalendarList([FromServices] IGoogleAuthProvider auth)
        {
            // Check if the required scopes have been granted. 
            if (await auth.RequireScopesAsync(CalendarService.ScopeConstants.CalendarReadonly) is IActionResult authResult)
            {
                // If the required scopes are not granted, then a non-null IActionResult will be returned,
                // which must be returned from the action. This triggers incremental authorization.
                // Once the user has granted the scope, an automatic redirect to this action will be issued.
                return authResult;
            }
            // The required scopes have now been granted.
            GoogleCredential cred = await auth.GetCredentialAsync();
            var service = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            var calendars = await service.CalendarList.List().ExecuteAsync();
            var calendarIds = calendars.Items.Select(calendar => calendar.Id).ToList();
            return View(calendarIds);
        }

        /// <summary>
        /// Fetches and shows the Google OAuth2 tokens that are currently active for the logged in user.
        /// Specifying the <see cref="AuthorizeAttribute"/> will guarantee that the code executes only if the
        /// user is authenticated. Once the user is authenticated the tokens are stored locally, in a cookie,
        /// and we can inspect them.
        /// </summary>
        [Authorize]
        public async Task<IActionResult> ShowTokens()
        {
            // The user is already authenticated, so this call won't trigger authentication.
            // But it allows us to access the AuthenticateResult object that we can inspect
            // to obtain token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(OpenIdConnectParameterNames.IdToken);
            string idTokenValid, idTokenIssued, idTokenExpires;
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(idToken);
                idTokenValid = "true";
                idTokenIssued = new DateTime(1970, 1, 1).AddSeconds(payload.IssuedAtTimeSeconds.Value).ToString();
                idTokenExpires = new DateTime(1970, 1, 1).AddSeconds(payload.ExpirationTimeSeconds.Value).ToString();
            }
            catch (Exception e)
            {
                idTokenValid = $"false: {e.Message}";
                idTokenIssued = "invalid";
                idTokenExpires = "invalid";
            }
            string accessToken = auth.Properties.GetTokenValue(OpenIdConnectParameterNames.AccessToken);
            string refreshToken = auth.Properties.GetTokenValue(OpenIdConnectParameterNames.RefreshToken);
            string accessTokenExpiresAt = auth.Properties.GetTokenValue("expires_at");
            string cookieIssuedUtc = auth.Properties.IssuedUtc?.ToString() ?? "<missing>";
            string cookieExpiresUtc = auth.Properties.ExpiresUtc?.ToString() ?? "<missing>";

            return View(new[]
            {
                $"Id Token: '{idToken}'",
                $"Id Token valid: {idTokenValid}",
                $"Id Token Issued UTC: '{idTokenIssued}'",
                $"Id Token Expires UTC: '{idTokenExpires}'",
                $"Access Token: '{accessToken}'",
                $"Refresh Token: '{refreshToken}'",
                $"Access token expires at: '{accessTokenExpiresAt}'",
                $"Cookie Issued UTC: '{cookieIssuedUtc}'",
                $"Cookie Expires UTC: '{cookieExpiresUtc}'",
            });
        }

        public class ForceTokenRefreshModel
        {
            public IReadOnlyList<string> Results;
            public string AccessToken;
        }

        /// <summary>
        /// Forces the refresh of the OAuth access token.
        /// Specifying the <see cref="AuthorizeAttribute"/> will guarantee that the code executes only if the
        /// user is authenticated.
        /// </summary>
        /// <param name="auth">The Google authorization provider.
        /// This can also be injected on the controller constructor.</param>
        [Authorize]
        public async Task<IActionResult> ForceTokenRefresh([FromServices] IGoogleAuthProvider auth)
        {
            // Obtain OAuth related values before the refresh.
            AuthenticateResult authResult0 = await HttpContext.AuthenticateAsync();
            string accessToken0 = authResult0.Properties.GetTokenValue(OpenIdConnectParameterNames.AccessToken);
            string refreshToken0 = authResult0.Properties.GetTokenValue(OpenIdConnectParameterNames.RefreshToken);
            string issuedUtc0 = authResult0.Properties.IssuedUtc?.ToString() ?? "<missing>";
            string expiresUtc0 = authResult0.Properties.ExpiresUtc?.ToString() ?? "<missing>";

            // Force token refresh by specifying a too-long refresh window.
            GoogleCredential cred = await auth.GetCredentialAsync(TimeSpan.FromHours(24));

            // Obtain OAuth related values after the refresh.
            AuthenticateResult authResult1 = await HttpContext.AuthenticateAsync();
            string accessToken1 = authResult1.Properties.GetTokenValue(OpenIdConnectParameterNames.AccessToken);
            string refreshToken1 = authResult1.Properties.GetTokenValue(OpenIdConnectParameterNames.RefreshToken);
            string issuedUtc1 = authResult1.Properties.IssuedUtc?.ToString() ?? "<missing>";
            string expiresUtc1 = authResult1.Properties.ExpiresUtc?.ToString() ?? "<missing>";

            // As demonstration compare the old values with the new ones and check that everything is
            // as it should be.
            string credAccessToken = await cred.UnderlyingCredential.GetAccessTokenForRequestAsync();

            bool accessTokenChanged = accessToken0 != accessToken1;
            bool credHasCorrectAccessToken = credAccessToken == accessToken1;

            bool pass = accessTokenChanged && credHasCorrectAccessToken;

            var model = new ForceTokenRefreshModel
            {
                Results = new[]
                {
                    $"Before Access Token: '{accessToken0}'",
                    $"Before Refresh Token: '{refreshToken0}'",
                    $"Before Issued UTC: '{issuedUtc0}'",
                    $"Before Expires UTC: '{expiresUtc0}'",
                    $"After Access Token: '{accessToken1}'",
                    $"After Refresh Token: '{refreshToken1}'",
                    $"After Issued UTC: '{issuedUtc1}'",
                    $"After Expires UTC: '{expiresUtc1}'",
                    $"Access token changed: {accessTokenChanged}",
                    $"Credential has correct access token: {credHasCorrectAccessToken}",
                    $"Pass: {pass}"
                },
                AccessToken = accessToken1
            };
            return View(model);
        }

        /// <summary>
        /// Checks that the access token is the expected one.
        /// Specifying the <see cref="AuthorizeAttribute"/> will guarantee that the code executes only if the
        /// user is authenticated.
        /// This method is used from the Force Refresh sample to show that the refreshed token is persisted.
        /// </summary>
        /// <param name="auth">The Google authorization provider.
        /// This can also be injected on the controller constructor.</param>
        /// <param name="expectedAccessToken">The expected token.</param>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ForceTokenRefreshCheckPersisted([FromServices] IGoogleAuthProvider auth, [FromForm] string expectedAccessToken)
        {
            // Check that the refreshed access token is correctly persisted across requests.
            var cred = await auth.GetCredentialAsync();
            var credAccessToken = await cred.UnderlyingCredential.GetAccessTokenForRequestAsync();
            var pass = credAccessToken == expectedAccessToken;
            return View(new[]
            {
                $"Expected access token: '{expectedAccessToken}'",
                $"Credential access token: '{credAccessToken}'",
                $"Pass: {pass}"
            });
        }
    }
}
