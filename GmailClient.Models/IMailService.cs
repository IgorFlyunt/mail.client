﻿using System.Threading.Tasks;

namespace GmailClient.Models
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);

        Task SendWelcomeEmailAsync(WelcomeRequest request);
    }
}
