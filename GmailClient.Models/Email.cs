﻿namespace GmailClient.Models
{
    public class Email
    {
        public string Title { get; set; }
        public string From { get; set; }
        //TODO: refactor into DateTime
        public string Date { get; set; }

        public bool IsReceived { get; set; }
        public string Id { get; set; }

        public string Body { get; set; }

    }
}
