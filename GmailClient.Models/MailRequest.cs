﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace GmailClient.Models
{
    public class MailRequest
    {
        [Required]
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        //public List<IFormFile> Attachments { get; set; }

        public string AttachmentPath { get; set; }
    }
}
