﻿using GmailClient.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.AspNetCore3;


namespace GmailClient.Services
{
    public class GmailService : IMailService
    {
        private readonly MailSettings _mailSettings;

        public GmailService(IOptions<MailSettings> mailSettings)
        {
            if (mailSettings != null)
            {
                _mailSettings = mailSettings.Value;
            }


        }

        public async Task SendEmailAsync(MailRequest mailRequest)
        {
            try
            {
                var email = new MimeKit.MimeMessage
                {
                    Sender = MimeKit.MailboxAddress.Parse(_mailSettings.Mail) //take igorflyuntdev
                };
                email.To.Add(MimeKit.MailboxAddress.Parse(mailRequest.ToEmail)); //take igorflyunt2
                email.Subject = mailRequest.Subject; //take theme
                var builder = new MimeKit.BodyBuilder();
                //TODO: check attachments
                //if (mailRequest.Attachments != null) //remove to another method or class
                //{
                //    foreach (var file in mailRequest.Attachments.Where(file => file.Length > 0))
                //    {
                //        byte[] fileBytes; //Ask Victor if this really needs to add filebytes in BodyBuilder, or his method builder.attachments can parse and add file without byte (read in documentation)
                //        await using (var ms = new MemoryStream())
                //        {
                //            await file.CopyToAsync(ms);
                //            fileBytes = ms.ToArray();
                //        }

                //        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                //    }
                //}

                builder.HtmlBody = mailRequest.Body; //Need logic to add html page to body, but it works without html, work with just text from view
                email.Body = builder.ToMessageBody();

                using var smtp = new SmtpClient();
                await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task SendWelcomeEmailAsync(WelcomeRequest request)
        {
            var filePath = Directory.GetCurrentDirectory() + "\\Templates\\WelcomeTemplate.html";
            var streamReader = new StreamReader(filePath);
            var mailText = await streamReader.ReadToEndAsync();
            streamReader.Close();
            mailText = mailText.Replace("[username]", request.UserName).Replace("[email]", request.ToEmail);
            var email = new MimeKit.MimeMessage
            {
                Sender = MimeKit.MailboxAddress.Parse(_mailSettings.Mail)
            };
            email.To.Add(MimeKit.MailboxAddress.Parse(request.ToEmail));
            email.Subject = $"Welcome {request.UserName}";
            var builder = new MimeKit.BodyBuilder();
            builder.HtmlBody = mailText;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            await smtp.AuthenticateAsync(_mailSettings.Mail, _mailSettings.Password);
            await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
        }




      
        public async Task<IList<Email>> GetEmails(string folder = "INBOX", int page = 1)
        {
            IList<Email> emails = new List<Email>();
            try
            {
                //TODO: Create logic in google.auth startup to save fileDataStore, and then save in DB
                var fileDataStore = new FileDataStore(GetType().ToString());

                var credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets
                    {
                        ClientId = "659796076770-kr33i3779pcb49g3p38kmk0d090080id.apps.googleusercontent.com",
                        ClientSecret = "SZGfp62dK33aeNWkXJCKP2K2"
                    },
                    new[]
                    {
                        Google.Apis.Gmail.v1.GmailService.Scope.GmailReadonly,
                        Google.Apis.Gmail.v1.GmailService.Scope.MailGoogleCom,
                        Google.Apis.Gmail.v1.GmailService.Scope.GmailModify
                    },
                    "user",
                    CancellationToken.None,
                    fileDataStore);



                var gmailService = new Google.Apis.Gmail.v1.GmailService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "GmailClient"
                });

                var emailListRequest = gmailService.Users.Messages.List("me");
                emailListRequest.LabelIds = folder;
                emailListRequest.IncludeSpamTrash = false;
                //emailListRequest.Q = "is:unread"; // This was added because I only wanted unread emails...

                // Get our emails
                var emailListResponse = await emailListRequest.ExecuteAsync();

                if (emailListResponse != null && emailListResponse.Messages != null)
                    // Loop through each email and get what fields I want...
                    foreach (var message in emailListResponse.Messages)
                    {
                        var messageInfoRequest = gmailService.Users.Messages.Get("me", message.Id);

                        // Make another request for that email id...
                        var messageInfoResponse = await messageInfoRequest.ExecuteAsync();

                        if (messageInfoResponse != null)
                        {

                            var from = "";
                            var date = "";
                            var subject = "";
                            var body = "";


                            // Loop through the headers and get the fields I need...
                            foreach (var messagePartHeader in messageInfoResponse.Payload.Headers)
                            {
                                switch (messagePartHeader.Name)
                                {
                                    case "Date":
                                        date = messagePartHeader.Value;
                                        break;
                                    case "From":
                                        from = messagePartHeader.Value;
                                        break;
                                    case "Subject":
                                        subject = messagePartHeader.Value;
                                        break;
                                }

                                if (date != "" && from != "")
                                {
                                    if (messageInfoResponse.Payload.Parts == null &&
                                        messageInfoResponse.Payload.Body != null)
                                    {
                                        body = messageInfoResponse.Payload.Body.Data;
                                        // Need to replace some characters as the data for the email's body is base64

                                        var codedBody = body.Replace("-", "+");
                                        codedBody = codedBody.Replace("_", "/");
                                        var data = Convert.FromBase64String(codedBody);
                                        body = Encoding.UTF8.GetString(data);
                                    }
                                    else
                                        body = GetDecodedNestedParts(messageInfoResponse.Payload.Parts, "");


                                    var email = new Email()
                                    {
                                        Id = message.Id,
                                        Date = date,
                                        From = from,
                                        Title = subject,
                                        IsReceived = true,
                                        Body = body
                                    };
                                    DateTime parsedDate;
                                    if (DateTime.TryParse(date, out parsedDate))
                                    {
                                        email.Date = parsedDate.ToShortDateString();
                                    }
                                    emails.Add(email);
                                }
                            }
                        }
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return emails;
        }

        public string GetDecodedNestedParts(IList<MessagePart> part, string data)
        {
            var bodyData = data;
            if (part == null) return bodyData;

            foreach (var messagePart in part)
                if (messagePart.Parts == null)
                {
                    if (messagePart.Body != null && messagePart.Body.Data != null)
                    {
                        var codedBody = messagePart.Body.Data.Replace("-", "+");
                        codedBody = codedBody.Replace("_", "/");
                        var decodedFromBase64 = Convert.FromBase64String(codedBody);
                        bodyData += Encoding.UTF8.GetString(decodedFromBase64);
                    }
                }
                else
                {
                    return GetDecodedNestedParts(messagePart.Parts, bodyData);
                }

            return bodyData;
        }



    }






}

